﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriathlonClassExample.Triathlon.Attendee.EventBehaviours;
using TriathlonClassExample.Triathlon.Event;

namespace TriathlonClassExample.Triathlon.Attendee
{
    interface ICompetitor : IAttendee
    {
        public ICompeteBehaviour CompeteBehaviour { get; set; }
        int Compete(IEvent triathlonEvent);
        public int SwimSkill { get; set; }
        public int CycleSkill { get; set; }
        public int RunSkill { get; set; }
    }
}
