﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriathlonClassExample.Triathlon.Attendee
{
    interface IAttendee
    {
        public string Name { get; set; }
    }
}
