﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriathlonClassExample.Triathlon.Attendee
{
    class Spectator : ISpectator
    {
        public string Name { get; set; }
    }
}
