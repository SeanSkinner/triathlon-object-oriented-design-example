﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriathlonClassExample.Triathlon.Event;

namespace TriathlonClassExample.Triathlon.Attendee.EventBehaviours
{
    interface ICompeteBehaviour
    {
        int Compete(IEvent triathlonEvent, ICompetitor competitor);
    }
}
