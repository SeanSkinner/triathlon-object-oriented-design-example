﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriathlonClassExample.Triathlon.Event;
using TriathlonClassExample.Triathlon.Util;

namespace TriathlonClassExample.Triathlon.Attendee.EventBehaviours
{
    class SwimCompeteBehaviour : ICompeteBehaviour
    {
        public int Compete(IEvent triathlonEvent, ICompetitor competitor)
        {
            if (triathlonEvent is not StandardSwimEvent)
            {
                throw new Exception("Event type is invalid for this behaviour");
            }

            StandardSwimEvent swimEvent = (StandardSwimEvent)triathlonEvent;

            return ScoreGenerator.GenerateScore() + (competitor.SwimSkill / swimEvent.SeaConditions);
        }
    }
}
