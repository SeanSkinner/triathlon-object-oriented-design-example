﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriathlonClassExample.Triathlon.Event;
using TriathlonClassExample.Triathlon.Util;

namespace TriathlonClassExample.Triathlon.Attendee.EventBehaviours
{
    class RunCompeteBehaviour : ICompeteBehaviour
    {
        public int Compete(IEvent triathlonEvent, ICompetitor competitor)
        {
            if (triathlonEvent is not StandardRunEvent)
            {
                throw new Exception("Event type is invalid for this behaviour");
            }

            return ScoreGenerator.GenerateScore() + competitor.RunSkill;
        }
    }
}
