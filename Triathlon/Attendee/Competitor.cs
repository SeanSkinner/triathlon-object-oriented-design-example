﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriathlonClassExample.Triathlon.Attendee.EventBehaviours;
using TriathlonClassExample.Triathlon.Event;

namespace TriathlonClassExample.Triathlon.Attendee
{
    class Competitor : ICompetitor
    {

        public int SwimSkill { get; set; }
        public int CycleSkill { get; set; }
        public int RunSkill { get; set; }
        public string Name { get; set; }
        public ICompeteBehaviour CompeteBehaviour { get; set; }

        public int Compete(IEvent triathlonEvent)
        {
            return CompeteBehaviour.Compete(triathlonEvent, this);
        }
    }
}
