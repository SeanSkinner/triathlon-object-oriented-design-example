﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriathlonClassExample.Triathlon.Util
{
    static class ScoreGenerator
    {
        private static Random rand = new Random();

        public static int GenerateScore()
        {
            return rand.Next(0, 101);
        }
    }
}
