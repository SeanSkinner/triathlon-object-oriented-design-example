﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriathlonClassExample.Triathlon.Attendee;

namespace TriathlonClassExample.Triathlon.Event
{
    class StandardSwimEvent : StandardEvent, ISeaEvent
    {
        public int SeaConditions { get; set; }

        public override string GetResultHeading()
        {
            return "Swim ";
        }
    }
}
