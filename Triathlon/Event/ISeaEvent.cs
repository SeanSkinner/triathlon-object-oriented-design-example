﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriathlonClassExample.Triathlon.Event
{
    interface ISeaEvent
    {
        public int SeaConditions { get; set; }
    }
}
