﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriathlonClassExample.Triathlon;
using TriathlonClassExample.Triathlon.Attendee;

namespace TriathlonClassExample.Triathlon.Event
{
    interface IEvent
    {
        public string Name { get; set; }
        public List<ICompetitor> Competitors { get; set; }
        public List<ISpectator> Spectators { get; set; }
        string GetEventResult();
        List<IAttendee> GetEventAttendees();
    }
}
