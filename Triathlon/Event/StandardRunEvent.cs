﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriathlonClassExample.Triathlon.Event
{
    class StandardRunEvent : StandardEvent
    {
        public override string GetResultHeading()
        {
            return "Run ";
        }
    }
}
