﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriathlonClassExample.Triathlon.Attendee;

namespace TriathlonClassExample.Triathlon.Event
{
    abstract class StandardEvent : IEvent
    {
        public string Name { get; set; }
        public List<ICompetitor> Competitors { get; set; }
        public List<ISpectator> Spectators { get; set; }

        public List<IAttendee> GetEventAttendees()
        {
            List<IAttendee> attendees = new List<IAttendee>();

            attendees.AddRange(Competitors);
            attendees.AddRange(Spectators);

            return attendees;
        }

        public string GetEventResult()
        {
            //initialize a collection to display our results
            Dictionary<ICompetitor, int> results = new Dictionary<ICompetitor, int>();

            //call compete on all our competitors in the event and store the result in said collection

            foreach (var competitor in Competitors)
            {
                results.Add(competitor, competitor.Compete(this));
            }

            string formattedResults = GetResultHeading() + "Event Results:\n" + string.Concat(results.OrderByDescending(r => r.Value)
                .Select(ce => $"{ce.Key.Name} had a score of {ce.Value}\n"));

            return formattedResults;
        }

        public abstract string GetResultHeading();
    }
}
