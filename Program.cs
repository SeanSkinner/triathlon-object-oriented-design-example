﻿using System;
using System.Collections.Generic;
using TriathlonClassExample.Triathlon.Attendee;
using TriathlonClassExample.Triathlon.Attendee.EventBehaviours;
using TriathlonClassExample.Triathlon.Event;

namespace TriathlonClassExample
{
    class Program
    {
        static void Main(string[] args)
        {
            //initialize all behaviours
            var swimBehaviour = new SwimCompeteBehaviour();
            var cycleBehaviour = new CycleCompeteBehaviour();
            var runBehaviour = new RunCompeteBehaviour();

            var competitors = new List<ICompetitor> { new Competitor { Name = "Fredrik", CycleSkill = 30, RunSkill = 50, SwimSkill = 60, CompeteBehaviour = swimBehaviour},
            new Competitor { Name = "Morten", CycleSkill = 70, RunSkill = 30, SwimSkill = 10, CompeteBehaviour = swimBehaviour },
            new Competitor { Name = "Wendy", CycleSkill = 90, RunSkill = 70, SwimSkill = 80, CompeteBehaviour = swimBehaviour}};

            var spectators = new List<ISpectator> { new Spectator { Name = "Sean" } };

            var swimEvent = new StandardSwimEvent { Name = "English Channel Swim Event", Competitors = competitors, Spectators = spectators, SeaConditions = 40 };

            Console.WriteLine(swimEvent.GetEventResult());

            ChangeCompeteBehaviour(competitors, cycleBehaviour);

            var cycleEvent = new StandardCycleEvent { Name = "Calais Endurance Cycle", Competitors = competitors, Spectators = spectators };
            Console.WriteLine(cycleEvent.GetEventResult());

            ChangeCompeteBehaviour(competitors, runBehaviour);

            var runEvent = new StandardRunEvent { Name = "Le Finale Run", Competitors = competitors, Spectators = spectators };
            Console.WriteLine(runEvent.GetEventResult());
        }

        static void ChangeCompeteBehaviour(List<ICompetitor> competitors, ICompeteBehaviour behaviour)
        {
            foreach (var competitor in competitors)
            {
                competitor.CompeteBehaviour = behaviour;
            }
        }
    }
}
